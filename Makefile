TEXFILE = MicroPythonNeuralNetwork

all: make_gv make_doc clean

make_gv:
	dot -Tpdf gv/NeuralNetworkDFF342.gv > gv/NeuralNetworkDFF342.pdf
	dot -Tpdf gv/NeuralNetworkDFF342Detail.gv > gv/NeuralNetworkDFF342Detail.pdf
	dot -Tpdf gv/NeuralNetworkDFF3452.gv > gv/NeuralNetworkDFF3452.pdf

make_doc:
	pdflatex ${TEXFILE}.tex
	pdflatex ${TEXFILE}.tex

clean:
	rm ${TEXFILE}.aux
	rm ${TEXFILE}.log
	rm ${TEXFILE}.out
