# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-02-09 14:48:09
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.14 and Python 3.7.3
# Project: Neural Network
# Description:

from matrix import Matrix
from math import exp
from random import random
from json import load, dump


class DFF(object):
    """Neural Network Deep Feed Forward (DFF)"""

    def __init__(self, layers=(1, 1), weights=None, func='sigmoid'):
        self.layers = layers
        self.func = func
        self.actfunc = getattr(DFF, func)
        self.actfunc_deriv = lambda a: getattr(DFF, func)(a, deriv=True)
        if weights:
            self.weights = weights
        else:
            self.weights = [
                self.randmat(*s) for s in zip(self.layers[:-1], self.layers[1:])
                ]

    @staticmethod
    def randmat(m, n):
        """Random Matrix m rows by n cols"""
        return Matrix([[random() for _ in range(n)] for _ in range(m)])

    @staticmethod
    def sigmoid(a, deriv=False):
        """Activation function, Sigmoid"""
        if deriv:
            return a * (1 - a)
        return 1 / (1 + exp(-a))

    @staticmethod
    def relu(a, deriv=False):
        """Activation function, Rectified Linear Unit (ReLU)"""
        if deriv:
            return 1 if a > 0 else 0
        return max(0, a)

    def predict(self, a):
        """Predict input: a"""
        for w in self.weights:
            a = (a @ w).map(self.actfunc)
        return a

    def train(self, a, s, lrate=1):
        """Feed a, forward error based on s and learning rate lrate"""
        # Gratient weights
        gweights = []
        # Layers
        layers = [a]
        # Feed
        for w in self.weights:
            layers.append((layers[-1] @ w).map(self.actfunc))
        # Forward
        hl = layers.pop()
        e = (s - hl) * hl.map(self.actfunc_deriv)
        for w in self.weights[::-1]:
            hl = layers.pop()
            gweights.append(hl.t() @ e)
            e = (e @ w.t()) * hl.map(self.actfunc_deriv)
        # Update weights
        for i, w in enumerate(self.weights):
            self.weights[i] += gweights.pop() * lrate

    def load(self, dff_file='DFFparm.json'):
        """Read data from json DFF file"""
        with open(dff_file) as f:
            parm = load(f)
        parm['weights'] = [Matrix(w) for w in parm['weights']]
        self.__init__(**parm)

    def dump(self, dff_file='DFFparm.json'):
        """Write data in json DFF file"""
        parameter = {
            'layers': self.layers,
            'weights': [w.mat for w in self.weights],
            'func': self.func
            }
        with open(dff_file, 'w') as f:
            dump(parameter, f)
