"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2021-04-30 15:54:43
License: MIT, Copyright (c) 2021 Olivier Lenoir
Language: Python 3.7.3
Project: Learn color
Description:
Reference:
Link:
    https://forum.micropython.org/viewtopic.php?f=2&t=7615
"""


from matrix import Matrix
from neuralnetwork import DFF
import csv


csv.register_dialect(
    'dsv',
    delimiter=';',
    lineterminator='\n',
    )


def short(a):
    return round(a, 3)


def convert(x, i_m=0, i_M=256, o_m=0, o_M=1):
    return (float(x) - i_m) * (o_M - o_m) / (i_M - i_m) + o_m


def getdata(afile='../data/colors.dsv'):
    with open(afile, 'r') as dsvfile:
        for rec in csv.DictReader(dsvfile, dialect='dsv'):
            yield rec


if __name__ == '__main__':
    color_map = {
        'Yellow': [1, 0, 0, 0, 0, 0],
        'Green': [0, 1, 0, 0, 0, 0],
        'Blue': [0, 0, 1, 0, 0, 0],
        'Red': [0, 0, 0, 1, 0, 0],
        'Orange': [0, 0, 0, 0, 1, 0],
        'Purple': [0, 0, 0, 0, 0, 1],
        }

    # Build training set
    training_set = []
    for d in getdata():
        training_set.append(
            [
                Matrix([list(map(convert, [d['Red'], d['Blue'], d['Green']]))]),
                Matrix([color_map.get(d['Color'])])
            ]
            )

    print('Training set')
    for ts in training_set:
        print(ts)
    print('=' * 40)

    nn = DFF((3, 4, 6))

    print('Intitial weigths')
    for w in nn.weights:
        print(w.map(short))

    print('=' * 20)
    print('Learning progress')
    for i in range(10000):
        for a, s in training_set:
            nn.train(a, s, 1)

    print()
    print('=' * 20)
    score = True
    for a, s in training_set:
        p = nn.predict(a)
        scr = str(p.map(round)) == str(s.map(short))
        print(a.map(short), p.map(short), p.map(round), s.map(short), scr)
        score &= scr
    print('Good learning?', score)

    print('=' * 20)
    print('Rounded weights')
    for i, w in enumerate(nn.weights):
        print('W{}'.format(i), w.map(round))

    nn.dump('test_color_neuralnetwork.json')
    nn.load('test_color_neuralnetwork.json')
    nn.dump('test_color_neuralnetwork_reload.json')
