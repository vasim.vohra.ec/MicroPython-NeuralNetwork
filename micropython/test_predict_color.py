"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2021-05-01 09:49:10
License: MIT, Copyright (c) 2021 Olivier Lenoir
Language: Python 3.7.3
Project: Test to predict color
Description:
Reference:
"""

color_map = {
    'Yellow': (1, 0, 0, 0, 0, 0),
    'Green': (0, 1, 0, 0, 0, 0),
    'Blue': (0, 0, 1, 0, 0, 0),
    'Red': (0, 0, 0, 1, 0, 0),
    'Orange': (0, 0, 0, 0, 1, 0),
    'Purple': (0, 0, 0, 0, 0, 1),
    }

inv_color_map = {
    (1, 0, 0, 0, 0, 0): 'Yellow',
    (0, 1, 0, 0, 0, 0): 'Green',
    (0, 0, 1, 0, 0, 0): 'Blue',
    (0, 0, 0, 1, 0, 0): 'Red',
    (0, 0, 0, 0, 1, 0): 'Orange',
    (0, 0, 0, 0, 0, 1): 'Purple',
    }

predict = (0, 0, 0, 0, 0, 1)

print(inv_color_map.get(predict, 'Oups'))
